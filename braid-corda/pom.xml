<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright 2018 Royal Bank of Scotland
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <parent>
    <artifactId>braid</artifactId>
    <groupId>io.bluebank.braid</groupId>
    <version>3.2.1-SNAPSHOT</version>
    <relativePath>../pom.xml</relativePath>
  </parent>

  <modelVersion>4.0.0</modelVersion>

  <artifactId>braid-corda</artifactId>
  <packaging>jar</packaging>

  <name>braid-corda</name>
  <description>a library for easy access to corda from non-jvm environments</description>

  <licenses>
    <license>
      <name>The Apache License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
    </license>
  </licenses>

  <scm>
    <connection>scm:git:git://gitlab.com/bluebank/braid.git</connection>
    <developerConnection>scm:git:ssh://gitlab.com:bluebank/braid.git</developerConnection>
    <url>http://gitlab.com/bluebank/braid/tree/master</url>
  </scm>

  <developers>
    <developer>
      <name>Farzad Pezeshkpour</name>
      <email>fuzz@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
    <developer>
      <name>Chris Jones</name>
      <email>chris.jones@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
    <developer>
      <name>Ben Wyeth</name>
      <email>ben.wyeth@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
    <developer>
      <name>Mark Simpson</name>
      <email>mark.simpson@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
    <developer>
      <name>Ramiz Amad</name>
      <email>ramiz.amad@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
  </developers>

  <dependencies>
    <dependency>
      <groupId>org.jetbrains.kotlin</groupId>
      <artifactId>kotlin-stdlib</artifactId>
      <version>${kotlin.version}</version>
    </dependency>
    <dependency>
      <groupId>org.jetbrains.kotlin</groupId>
      <artifactId>kotlin-test-junit</artifactId>
      <version>${kotlin.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>${junit.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.bluebank.braid</groupId>
      <artifactId>braid-core</artifactId>
      <version>${project.version}</version>
    </dependency>
    <dependency>
      <groupId>io.swagger</groupId>
      <artifactId>swagger-core</artifactId>
      <version>1.5.18</version>
    </dependency>
    <dependency>
      <groupId>net.corda</groupId>
      <artifactId>corda-node</artifactId>
      <version>${corda.version}</version>
      <exclusions>
        <exclusion>
          <groupId>net.corda.plugins</groupId>
          <artifactId>cordform-common</artifactId>
        </exclusion>
        <!--<exclusion>-->
          <!--<groupId>com.github.corda.crash</groupId>-->
          <!--<artifactId>crash.shell</artifactId>-->
        <!--</exclusion>-->
        <!--<exclusion>-->
          <!--<groupId>com.github.corda.crash</groupId>-->
          <!--<artifactId>crash.connectors.ssh</artifactId>-->
        <!--</exclusion>-->
        <!--<exclusion>-->
          <!--<groupId>com.github.bft-smart</groupId>-->
          <!--<artifactId>library</artifactId>-->
        <!--</exclusion>-->
      </exclusions>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>io.vertx</groupId>
      <artifactId>vertx-auth-jwt</artifactId>
      <version>${vertx.version}</version>
    </dependency>
    <dependency>
      <groupId>javax.ws.rs</groupId>
      <artifactId>javax.ws.rs-api</artifactId>
      <version>2.1</version>
    </dependency>
    <dependency>
      <groupId>net.corda</groupId>
      <artifactId>corda-node-driver</artifactId>
      <version>${corda.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>net.corda</groupId>
      <artifactId>corda-test-utils</artifactId>
      <version>${corda.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.vertx</groupId>
      <artifactId>vertx-unit</artifactId>
      <version>${vertx.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <sourceDirectory>src/main/kotlin</sourceDirectory>
    <testSourceDirectory>src/test/kotlin</testSourceDirectory>

    <plugins>
      <plugin>
        <groupId>org.jetbrains.kotlin</groupId>
        <artifactId>kotlin-maven-plugin</artifactId>
        <version>${kotlin.version}</version>
        <executions>
          <execution>
            <id>compile</id>
            <phase>compile</phase>
            <goals>
              <goal>compile</goal>
            </goals>
          </execution>
          <execution>
            <id>test-compile</id>
            <phase>test-compile</phase>
            <goals>
              <goal>test-compile</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <jvmTarget>1.8</jvmTarget>
          <javaParameters>true</javaParameters>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <argLine>-javaagent:${basedir}/../lib/quasar.jar</argLine>
        </configuration>
      </plugin>
      <!--<plugin>-->
        <!--<groupId>org.apache.maven.plugins</groupId>-->
        <!--<artifactId>maven-shade-plugin</artifactId>-->
        <!--<version>3.1.0</version>-->
        <!--<executions>-->
          <!--<execution>-->
            <!--<phase>package</phase>-->
            <!--<goals>-->
              <!--<goal>shade</goal>-->
            <!--</goals>-->
            <!--<configuration>-->
              <!--<relocations>-->
                <!--<relocation>-->
                  <!--<pattern>io.netty</pattern>-->
                  <!--<shadedPattern>io.bluebank.braid.shaded.io.netty</shadedPattern>-->
                <!--</relocation>-->
                <!--<relocation>-->
                  <!--<pattern>com.fasterxml</pattern>-->
                  <!--<shadedPattern>io.bluebank.braid.shaded.com.fasterxml</shadedPattern>-->
                <!--</relocation>-->
                <!--<relocation>-->
                  <!--<pattern>io.swagger</pattern>-->
                  <!--<shadedPattern>io.bluebank.braid.shaded.io.swagger</shadedPattern>-->
                <!--</relocation>-->
                <!--<relocation>-->
                  <!--<pattern>rx</pattern>-->
                  <!--<shadedPattern>io.bluebank.braid.shaded.rx</shadedPattern>-->
                <!--</relocation>-->
                <!--<relocation>-->
                  <!--<pattern>io.vertx</pattern>-->
                  <!--<shadedPattern>io.bluebank.braid.shaded.io.vertx</shadedPattern>-->
                <!--</relocation>-->
                <!--<relocation>-->
                  <!--<pattern>org.apache</pattern>-->
                  <!--<shadedPattern>io.bluebank.braid.shaded.org.apache</shadedPattern>-->
                <!--</relocation>-->
              <!--</relocations>-->
              <!--<transformers>-->
                <!--<transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer"/>-->
              <!--</transformers>-->
            <!--</configuration>-->
          <!--</execution>-->
        <!--</executions>-->
      <!--</plugin>-->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <executions>
          <execution>
            <id>attach-sources</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>3.0.0-M1</version>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <reportSets>
          <reportSet>
            <inherited>false</inherited>
            <reports/>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>kr.motd.maven</groupId>
        <artifactId>sphinx-maven-plugin</artifactId>
        <version>2.2.2</version>
        <reportSets>
          <reportSet>
            <inherited>false</inherited>
          </reportSet>
        </reportSets>
      </plugin>
    </plugins>
  </reporting>
</project>
